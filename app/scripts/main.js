'use strict';
console.log('\'Allo \'Allo!');

/**
 * Модуль виджета галлереи
 */
(function () {
    var app = app || {};
    var widgetClassName = 'flexslider';

    app.gallery = function () {
        this.elems = $('.' + widgetClassName);
        if(this.elems.length) {
            $.each(this.elems, function (index, item) {
                var $item = $(item);
                var $nav_prev = $item.parent().find('.b-widget-gallery_nav_prev');
                var $nav_next = $item.parent().find('.b-widget-gallery_nav_next');
                var $nav_counter = $item.parent().find('.b-widget-gallery_nav_counter');
                var $desc = $item.parent().find('.b-widget-gallery_desc');
                $item.parent().addClass('loaded');
                var $slider = $item.flexslider({
                    animation: "slide",
                    controlNav: false,
                    directionNav: false,
                    init: function (slider){
                        this.setCounter(slider);
                        this.setDescription(slider);
                    },
                    start: function (slider){
                        $nav_prev.on('click', function () {
                            $item.flexslider("prev");
                        });
                        $nav_next.on('click', function () {
                            $item.flexslider("next");
                        });
                    },
                    after: function (slider) {
                        this.setCounter(slider);
                        this.setDescription(slider);
                    },
                    /**
                     * Изменяет счетчик текущего слайдера в DOM
                     * @param slider
                     */
                    setCounter: function (slider) {
                        $nav_counter.text(slider.currentSlide + ' / ' + slider.count);
                    },
                    /**
                     * Изменяет описание в DOM
                     * @param slider
                     */
                    setDescription: function (slider) {
                        $desc.text(slider.slides[slider.currentSlide].attributes['data-desc'].value);
                    }
                });
            });
        }
    };
    app.gallery.prototype = {
        elems: null
    };
    window.app = $.extend(window.app || {}, app);
})();

/**
 * Модуль виджета аккордиона
 */
$(function () {
    var app = app || {};

    app.accordion = function (el) {
        this.el = el;
        this.$el = $(el);

        var allPanels = this.$el.find('.b-menu-accordion_submenu');
        $.each(allPanels, function (index, item) {
            var $item = $(item);
            if(!$item.parent().hasClass('__active')) {
                $item.hide();
            }
        });

        this.$el.find('.b-menu-accordion_item_name').click(function() {
            if(!$(this).parent().hasClass('__active')) {
                allPanels.slideUp().parent().removeClass('__active');
                $(this).parent().addClass('__active').find('.b-menu-accordion_submenu').slideDown();
            } else {
                allPanels.slideUp().parent().removeClass('__active');
            }
            return false;
        });
    };

    window.app = $.extend(window.app, app);
});

$(window).load(function() {
    new app.gallery();

    var allPanels = $('.b-menu-accordion');
    allPanels.each(function (index, item) {
        new app.accordion(item);
    });
});