//
// Анимация паралакса
var controller = new ScrollMagic();
var tween = new TimelineMax()
    .add([
        TweenMax.fromTo(".b-index_hello", 1,{backgroundPosition: "50% 0%"}, {backgroundPosition: "50% 100%", ease: Linear.easeNone})
    ]);
var duration = $(window).height();
var sceneHello = new ScrollScene({duration: duration})
    .setTween(tween)
    .addTo(controller);
//sceneHello.addIndicators();
var tween = new TimelineMax()
    .add([
        TweenMax.fromTo(".b-index_projects", 1,{backgroundPosition: "50% 0%"}, {backgroundPosition: "50% 100%", ease: Linear.easeNone})
    ]);
var duration = $(window).height() + $('.b-index_projects').outerHeight();
var sceneProjects = new ScrollScene({duration: duration, triggerElement: $('.b-index_projects'), offset: $(window).height()/2 * -1 })
    .setTween(tween)
    .addTo(controller);
//sceneProjects.addIndicators();

//
// Валидация формы
$.validate({
    validateOnBlur : false,
    onSuccess : function() {
        alert('The form is valid!');
        return false; // Will stop the submission of the form
    },
    onValidate : function() {
        return {
            element : $('#some-input'),
            message : 'This input has an invalid value for some reason'
        }
    }
});